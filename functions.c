//Lukasz Dzwoniarek
//274404

#include "functions.h"
#include <netinet/ip.h>

char ip_adress[3][16];
struct timeval time_rec[3];

u_int16_t compute_icmp_checksum (const void *buff, int length){
    u_int32_t sum;
    const u_int16_t* ptr = buff;
    assert (length % 2 == 0);
    for (sum = 0; length > 0; length -= 2)
        sum += *ptr++;
    sum = (sum >> 16) + (sum & 0xffff);
    return (u_int16_t)(~(sum + (sum >> 16)));
}

void error(const char* messege){
    fprintf(stderr, "%s", messege);
    exit(1);
}

void Gettimeofday(struct timeval *tv){
    gettimeofday(tv, NULL);
}

int receive(int ttl, int sock, pid_t proc_id){
    struct sockaddr_in sender;
    socklen_t sender_len = sizeof(sender);
    u_int8_t buffer[IP_MAXPACKET+1];

    int n = recvfrom(sock, buffer, IP_MAXPACKET, MSG_DONTWAIT, (struct sockaddr*)&sender, &sender_len);
    if(n == -1){
            return 0;
    }

    struct timeval rec;
    Gettimeofday(&rec);

    struct iphdr *ip_hdr;
    int ip_hlen;
    ip_hdr = (struct iphdr*) buffer;
    ip_hlen = (ip_hdr->ihl) * 4;

    struct icmphdr *icmp_hdr;
    unsigned int icmp_len;
    icmp_hdr = (struct icmphdr*) (buffer + ip_hlen);

    if((icmp_len = n-ip_hlen) < 8){
        return 0;
    }

    if(icmp_hdr->type == ICMP_TIME_EXCEEDED && icmp_hdr->code == ICMP_EXC_TTL){
        if(icmp_len < 8 + sizeof(struct iphdr)){
            return 0;
        }
        struct iphdr *hip;
        hip = (struct iphdr*) (buffer + ip_hlen + 8);
        unsigned int ip_hlen2 = (hip->ihl) * 4;

        if(icmp_len < 8 + ip_hlen2 + 8){
            return 0;
        }
        if(hip->protocol == IPPROTO_ICMP){
            struct icmphdr *orig = (struct icmphdr*) (buffer + ip_hlen + 8 + ip_hlen2);

            pid_t id = ntohs(orig->un.echo.id);
            int seq = ntohs(orig->un.echo.sequence);
            if(id != proc_id){
                return 0;
            }

            if(seq/3 != ttl){
                return 0;
            }
            time_rec[seq%3].tv_sec = rec.tv_sec;
            time_rec[seq%3].tv_usec = rec.tv_usec;

            inet_ntop(AF_INET, &(sender.sin_addr), ip_adress[seq%3], sizeof(ip_adress[seq%3]));

            return 1;
        }
        else{
            return 0;
        }
    }

    if(icmp_hdr->type == ICMP_ECHOREPLY){
        pid_t id = ntohs(icmp_hdr->un.echo.id);
        int seq = ntohs(icmp_hdr->un.echo.sequence);

        if(id != proc_id){
            return 0;
        }

        if(seq/3 != ttl){
            return 0;
        }
        time_rec[seq%3].tv_sec = rec.tv_sec;
        time_rec[seq%3].tv_usec = rec.tv_usec;

        inet_ntop(AF_INET, &(sender.sin_addr), ip_adress[seq%3], sizeof(ip_adress[seq%3]));

        return -1;
    }
    return 0;
}

u_int64_t time_diff(struct timeval start, struct timeval end){
    return ((end.tv_sec - start.tv_sec) * 1000) + ((end.tv_usec - start.tv_usec) / 1000);
}

bool time_comp(struct timeval start, struct timeval end){  //0 jesli adresy identyczne
    if(end.tv_sec > start.tv_sec){
        return true;
    }
    if(end.tv_usec > start.tv_usec){
        return true;
    }
    return false;
}

void print(){
    u_int64_t div[3], result=0, recived=0;
    for(int i=0; i<3; i++){
        div[i] = time_diff(time_send[i], time_rec[i]);
        if( time_comp(time_send[i], time_rec[i]) ){
            result += div[i];
            recived++;
        }
    }

    if(recived == 0){
        printf("*\n");
        return;
    }

    bool print;
    for(int i = 0; i<3; i++){
        if(time_comp(time_send[i], time_rec[i])){
            print = true;
            for(int j = 0; j<i; j++){
                if(time_comp(time_send[j], time_rec[j]) && strcmp(ip_adress[i], ip_adress[j]) == 0){
                    print = false;
                }
            }
            if(print){
                printf("%s ", ip_adress[i]);
            }
        }
    }
    if(recived == 3){
        result /= recived;
        printf("%" PRIu64 "ms\n", result);
    }
    else{
        printf("???\n");
    }
}
