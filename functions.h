//Lukasz Dzwoniarek
//274404

#ifndef _func_h_

#define _func_h_

#include <sys/types.h>
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <errno.h>
#include <netinet/ip_icmp.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#include <inttypes.h>

extern struct timeval time_send[3];

u_int16_t compute_icmp_checksum (const void *buff, int length);
void error(const char* x);
void Gettimeofday(struct timeval *tv);
int receive(int ttl, int sock, pid_t proc_id);
u_int64_t time_diff(struct timeval start, struct timeval end);
void print();

#endif
