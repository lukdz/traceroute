CC=gcc
CFLAGS=-std=gnu99 -Wall -W
EXECUTABLE = traceroute

all: program

program: main.o functions.o
	$(CC) main.o functions.o -o $(EXECUTABLE)

main.o: main.c
	$(CC) -c $(CFLAGS) main.c

functions.o: functions.c
	$(CC) -c $(CFLAGS) functions.c

distclean:
	rm *.o $(EXECUTABLE)

clean:
	rm *.o
