//Lukasz Dzwoniarek
//274404

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <strings.h>
#include <sys/time.h>
#include <netinet/ip_icmp.h>

#include "functions.h"

struct timeval time_send[3];

int main(int argc, char *argv[]){
    if(argc != 2){
        error("Jako argument podaj tylko docelowy adres IP");
    }

    struct sockaddr_in target;
    bzero(&target, sizeof(struct sockaddr_in));
    target.sin_family = AF_INET;
    if(inet_pton(AF_INET, argv[1], &target.sin_addr) == 0){
        error("Nieprawidlowy adres IP, poprawny np. 8.8.8.8");
    }

    int sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
    if(sock == -1){
        error("Nie utworzona gniazda, uruchom program z sudo");
    }

    bool odp = true;
    for(int TTL = 1; TTL < 60 && odp; TTL++){
        setsockopt(sock, IPPROTO_IP, IP_TTL, &TTL, sizeof(int));

        for(int mess = 0; mess < 3; mess++){
            struct icmphdr header;
            bzero(&header, sizeof(struct icmphdr));
            header.type = ICMP_ECHO;
            header.un.echo.id = htons(getpid());
            header.un.echo.sequence = htons( (TTL*3) + mess );
            header.checksum = compute_icmp_checksum((u_int16_t*)&header, sizeof(struct icmphdr));

            sendto(sock, &header, sizeof(struct icmphdr), 0, (struct sockaddr*)&target, sizeof(struct sockaddr_in));
            Gettimeofday(&time_send[mess]);
        }

        struct timeval time_start=time_send[2], time_now;
        int mess=0, me;
        do{
            me = receive(TTL, sock, getpid());
            mess += abs(me);
            Gettimeofday(&time_now);
            if(me == -1){
                odp = false;
            }
        }while(time_diff(time_start, time_now) < 1000 && mess<3);

        printf("%d ", TTL);
        print();
    }

    return 0;
}
